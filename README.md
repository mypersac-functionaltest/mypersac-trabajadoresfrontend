# Proyecto en ReactJs para el consumo de [MyPerSac-TrabajadoresBackend](https://gitlab.com/mypersac-functionaltest/mypersac-trabajadoresbackend)


## Video demostrativo del funcionamiento
[![Video](https://i3.ytimg.com/vi/-C2XZqe46MY/maxresdefault.jpg)](https://youtu.be/-C2XZqe46MY)
**Click en la imagen para ir al video**

Antes de todo debemos instalar las dependencias de nuestro proyecto, para ello abriremos una terminal en el proyecto y ejecutaremos el siguiente comando:
```bash
npm install
``` 
Una vez instalada las dependencias de nuestro proyecto debemos configurar algunas variables de entorno. Para ello debemos crear el archivo ".env.local" en la raiz de nuestro poryecto.<br><br>
![imagen](/uploads/18498379cb774d2b827159619ffffc51/imagen.png)<br>
*Nota: Este archivo nunca se subirá al repocitorio*

Una vez creado, lo abriremos y escribiremos nuestra variable de entorno para definir la ruta de la API a la cual se conectará para realizar todas las peticiones HTTP.
```env
VITE_API_URL=https://localhost:<tu-puerto>/api
```
Guardaremos cambios y cerraremos el archivo :+1:<br><br>
:eyes: Debemos tener en cuenta que la url de nuestro proyecto debe ser igual al que configuramos en los CORS del proyecto backend [MyPerSac-TrabajadoresBackend](https://gitlab.com/mypersac-functionaltest/mypersac-trabajadoresbackend):link:<br><br>
Una vez realizados todos estos pasos podremos ejecutar nuestro proyecto y consumir la API hecha en C# correctamente :smile:<br><br>
Para iniciar el proyecto debemos ejecutar el siguiente comando en la terminal:
```bash
npm run dev
```
Debemos observar el siguiente resultado:<br>
![imagen](/uploads/6b94c1122e90bfa0bfb727e8b67e15e0/imagen.png)<br>
*Ejecución de la aplicación sin errores (yo tengo pnpm pero si usted no lo tiene solo utilice npm como se indico anteriormente)*
<br><br>
Ahora si, abramos la ruta en el navegador (De preferencia utilizar localhost en lugar de 127.0.0.1, justo como en la configuracion cors del proyecto backend [MyPerSac-TrabajadoresBackend](https://gitlab.com/mypersac-functionaltest/mypersac-trabajadoresbackend):link:)
<br>
## GRACIAS! :smile:
