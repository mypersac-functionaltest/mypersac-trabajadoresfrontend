import { useState } from "react";
import { IDepartamento, IDistrito, IProvinica } from "../interfaces/ubigeo.interface";
import ubigeoService from "../services/ubigeo.service";


export default function useUbigeo() {
  const [departamentos, setDepartamentos] = useState<IDepartamento[] | null>(null)
  const [provincias, setProvincias] = useState<IProvinica[] | null>(null)
  const [distritos, setDistritos] = useState<IDistrito[] | null>(null)


  const getDepartamentos = async () => {
    const departamentos = await ubigeoService.getDepartamentos() 
    setDepartamentos(departamentos)
  }

  const getProvincias = async (id_departamento: string) => {
    const provincias = await ubigeoService.getProvinciasByDepartamentoId(id_departamento)
    setProvincias(provincias)
  }

  const getDistritos = async (id_provincia: string) => {
    const distritos = await ubigeoService.getDistritosByProvinciaId(id_provincia)
    setDistritos(distritos)
  }

  const clearProvincias = () => {
    setProvincias(null)
  }

  const clearDistritos = () => {
    setDistritos(null)
  }

  return {
    departamentos,
    provincias,
    distritos,
    getDepartamentos,
    getProvincias,
    getDistritos,
    clearProvincias,
    clearDistritos
  }


}