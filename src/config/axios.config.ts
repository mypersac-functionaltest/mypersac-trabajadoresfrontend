import axios from "axios";

const apiBase = axios.create({
  baseURL: import.meta.env.VITE_API_URL
})

export default apiBase