import * as Yup from "yup"


export const trabajadorSchemeValidatior = Yup.object().shape({
  tipoDocumento: Yup.string().required("Debe seleccionar un tipo de documento"),
  numeroDocumento: Yup.string().matches(/^[0-9]+$/,"Debe ingresar números").required("Campo requerido"),
  nombres: Yup.string().required("Campo requerido"),
  sexo: Yup.string().matches(/^(M|F)$/,"Formato incorrecto").required("Debe seleccionar el sexo"),
  idDepartamento: Yup.string().matches(/^[0-9]+$/,"Formato de id inválido").required("Debe seleccionar un departamento"),
  idProvincia: Yup.string().matches(/^[0-9]+$/,"Formato de id inválido").required("Debe seleccionar una provincia"),
  idDistrito: Yup.string().matches(/^[0-9]+$/,"Formato de id inválido").required("Debe seleccionar un distrito"),
})