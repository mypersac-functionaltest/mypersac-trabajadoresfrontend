import { AxiosResponse } from "axios"
import apiBase from "../config/axios.config"
import { ApiResponse } from "../interfaces/api-response.interface"
import { ITrabajador, ITrabajadorById, ITrabajadorForm } from "../interfaces/trabajador.interface"

const getTrabajadorById = async (id: string) => {
  const {data: response}:AxiosResponse<ApiResponse<ITrabajadorById>> = await apiBase.get(`/Trabajador/${id}`)
  return response.data
}

const getTrabajadores = async () => {
  const {data: response}:AxiosResponse<ApiResponse<ITrabajador[]>> = await apiBase.get('/Trabajador')
  return response.data
}

const getTrabajadorBySexo = async (id: string) => {
  const {data: response}:AxiosResponse<ApiResponse<ITrabajador[]>> = await apiBase.get(`/Trabajador/By-sexo?sexo=${id}`)
  return response.data
}

const addTrabajador = async (trabajador: ITrabajadorForm) => {
  const {data: response}:AxiosResponse<ApiResponse<ITrabajador>> = await apiBase.post('/Trabajador',{
    ...trabajador,
    idDepartamento: Number(trabajador.idDepartamento),
    idProvincia: Number(trabajador.idProvincia),
    idDistrito: Number(trabajador.idDistrito),
  } as ITrabajadorForm)
  return response.data
}

const updateTrabajador = async (trabajador: ITrabajadorForm, id: string) => {
  const {data: response}:AxiosResponse<ApiResponse<ITrabajador>> = await apiBase.put(`/Trabajador/${id}`,{
    ...trabajador,
    idDepartamento: Number(trabajador.idDepartamento),
    idProvincia: Number(trabajador.idProvincia),
    idDistrito: Number(trabajador.idDistrito),
  } as ITrabajadorForm)
  return response.data
}

const deletTrabajadorById = async (id: string) => {
  const {data: response}:AxiosResponse<ApiResponse<ITrabajadorById>> = await apiBase.delete(`/Trabajador/${id}`)
  return response.data;
}

const trabajadorService ={
  getTrabajadorById,
  getTrabajadores,
  getTrabajadorBySexo,
  addTrabajador,
  updateTrabajador,
  deletTrabajadorById
}

export default trabajadorService