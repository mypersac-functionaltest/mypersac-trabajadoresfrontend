import { AxiosResponse } from "axios"
import { ApiResponse } from "../interfaces/api-response.interface"
import { IDepartamento, IDistrito, IProvinica } from "../interfaces/ubigeo.interface"
import apiBase from "../config/axios.config"


const getDepartamentos = async () => {
  const {data: response}: AxiosResponse<ApiResponse<IDepartamento[]>> = await apiBase.get('Departamento') 
  return response.data
}

const getProvinciasByDepartamentoId = async (id_departamento: string) => {
  const {data: response}: AxiosResponse<ApiResponse<IProvinica[]>> = await apiBase.get(`Departamento/${id_departamento}/provincia`) 
  return response.data
}

const getDistritosByProvinciaId = async (id_provincia: string) => {
  const {data: response}: AxiosResponse<ApiResponse<IDistrito[]>> = await apiBase.get(`Provincia/${id_provincia}/distrito`) 
  return response.data
} 

const ubigeoService = {
  getDepartamentos,
  getProvinciasByDepartamentoId,
  getDistritosByProvinciaId
}

export default ubigeoService