import { type FC } from 'react';
import { ITrabajador } from '../interfaces/trabajador.interface';
import clsx from 'clsx';

interface TablaTrabajadorProps {
  openModal: (id: number) => void,
  trabajadores: ITrabajador[] | null,
  handleDelete: (id: number) => void
}

const TablaTrabajador: FC<TablaTrabajadorProps> = ({ openModal, trabajadores, handleDelete }) => {

  return (
    <>
      <div className='table-responsive'>
        <table className="table">
          <thead className='bg-dark text-white'>
            <tr>
              <th>Tipo Documento </th>
              <th>Nro documento</th>
              <th>Nombres</th>
              <th>Sexo</th>
              <th>Departamento</th>
              <th>Provincia</th>
              <th>Distrito</th>
              <th className='text-center'>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {
              trabajadores ?
                trabajadores.map(t => (
                  <tr key={t.id} className={clsx(t.sexo === 'M' ?'male-table-color': 'female-table-color')}>
                    <th>{t.tipoDocumento}</th>
                    <td>{t.numeroDocumento}</td>
                    <td>{t.nombres}</td>
                    <td>{t.sexo}</td>
                    <td>{t.nombreDepartamento}</td>
                    <td>{t.nombreProvincia}</td>
                    <td>{t.nombreDistrito}</td>
                    <td>
                      <div className='d-flex gap-3'>
                        <button className='btn btn-warning border border-2 border-white' onClick={() => openModal(t.id)}>Editar</button>
                        <button className='btn btn-danger border border-2 border-white' onClick={() => handleDelete(t.id)}>Eliminar</button>
                      </div>
                    </td>
                  </tr>
                ))
                : null
            }
          </tbody>
        </table>
      </div>
    </>
  );
}
export default TablaTrabajador;