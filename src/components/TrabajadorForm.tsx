/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, type FC } from 'react';
import { Formik, Field, ErrorMessage, Form, FormikHelpers } from "formik";
import { ITrabajadorForm } from '../interfaces/trabajador.interface';
import { AnyObject, ObjectSchema } from "yup";
import useUbigeo from '../hooks/useUbigeo';

interface TrabajadorFormProps {
  handleSubmit: (values: ITrabajadorForm, helpers: FormikHelpers<ITrabajadorForm>) => void
  initialValues: ITrabajadorForm
  validationSchema: ObjectSchema<AnyObject>
  handleClose: () => void
}


const TrabajadorForm: FC<TrabajadorFormProps> = ({ handleSubmit, initialValues, validationSchema, handleClose }) => {

  const { departamentos, getDepartamentos, provincias, getProvincias, distritos, getDistritos, clearProvincias, clearDistritos } = useUbigeo()

  const onChangeDepartament = (e: any, setField: any) => {
    setField('idDepartamento', e.target.value)
    e.target.value !== "" && getProvincias(e.target.value)

    clearProvincias()
    setField("idProvincia", "")

    clearDistritos()
    setField("idDistrito", "")
  }

  const onChangeProvincia = (e: any, setField: any) => {
    setField('idProvincia', e.target.value)
    e.target.value !== "" && getDistritos(e.target.value)
    clearDistritos()
    setField("idDistrito", "")
  }

  useEffect(() => {
    getDepartamentos()
  }, [getDepartamentos])

  useEffect(() => {
    if(initialValues.idDepartamento !== "") {
      getProvincias(initialValues.idDepartamento.toString())
      getDistritos(initialValues.idProvincia.toString())
    }
  },[])

  return (
    <Formik
      onSubmit={handleSubmit}
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      {
        ({ isSubmitting, setFieldValue }) => (
          <Form className='d-flex flex-column gap-3'>
            <div>
              <label className='form-label fw-semibold text-dark'>Tipo de documento</label>
              <Field name="tipoDocumento" as="select" className="form-select">
                <option value="" defaultChecked>Seleccionar...</option>
                <option value="DNI">DNI</option>
              </Field>
              <ErrorMessage name="tipoDocumento" component="span" className="text-danger" />
            </div>
            <div>
              <label className='form-label fw-semibold text-dark'>Numero del documento</label>
              <Field name="numeroDocumento" className="form-control" />
              <ErrorMessage name="numeroDocumento" component="span" className="text-danger" />
            </div>
            <div>
              <label className='form-label fw-semibold text-dark'>Nombres</label>
              <Field name="nombres" className="form-control" />
              <ErrorMessage name="nombres" component="span" className="text-danger" />
            </div>
            <div>
              <label className='form-label fw-semibold text-dark'>Sexo</label>
              <Field name="sexo" as="select" className="form-select">
                <option value="" defaultChecked>Seleccionar...</option>
                <option value="M">Masculino</option>
                <option value="F">Femenino</option>
              </Field>
              <ErrorMessage name="sexo" component="span" className="text-danger" />
            </div>
            <div>
              <label className='form-label fw-semibold text-dark'>Departamento</label>
              <Field name="idDepartamento" as="select" className="form-select" onChange={(e: any) => onChangeDepartament(e, setFieldValue)}>
                <option value="" defaultChecked>Seleccionar...</option>
                {
                  departamentos && departamentos.map(d => <option key={d.id} value={d.id}>{d.nombreDepartamento}</option>)
                }
              </Field>
              <ErrorMessage name="idDepartamento" component="span" className="text-danger" />
            </div>
            <div>
              <label className='form-label fw-semibold text-dark'>Provincia</label>
              <Field name="idProvincia" as="select" className="form-select" onChange={(e: any) => onChangeProvincia(e, setFieldValue)}>
                <option value="" defaultChecked>Seleccionar...</option>
                {
                  provincias && provincias.map(p => <option key={p.id} value={p.id}>{p.nombreProvincia}</option>)
                }
              </Field>
              <ErrorMessage name="idProvincia" component="span" className="text-danger" />
            </div>
            <div>
              <label className='form-label fw-semibold text-dark'>Distrito</label>
              <Field name="idDistrito" as="select" className="form-select" >
                <option value="" defaultChecked>Seleccionar...</option>
                {
                  distritos && distritos.map(d => <option key={d.id} value={d.id}>{d.nombreDistrito}</option>)
                }
              </Field>
              <ErrorMessage name="idDistrito" component="span" className="text-danger" />
            </div>
            <div className='d-flex w-100 gap-2 mt-3 flex-wrap'>
              <button type='submit' className='btn btn-primary w-100' disabled={isSubmitting}>Enviar</button>
              <button className='btn btn-danger w-100' onClick={handleClose}>Cancelar</button>
            </div>
          </Form>
        )
      }
    </Formik>
  );
}
export default TrabajadorForm;