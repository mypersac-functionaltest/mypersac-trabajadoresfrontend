import React, { useEffect } from "react";
import { createPortal } from "react-dom";

interface ModalProps {
  children: React.ReactNode
  isOpen: boolean
}

const Modal: React.FC<ModalProps> = ({ children, isOpen }) => {

  useEffect(() => {
    if (isOpen) {
      document.querySelector('body')?.classList.add('no-scroll')
    }else{
      document.querySelector('body')?.classList.remove('no-scroll')
    }
  },[isOpen])

  return (
    <>
      {
        isOpen ?
          createPortal(
            (
              <div className="modal-back">
                <div className="modal-body">
                  {children}
                </div>
              </div>
            ),
            document.querySelector('#modal-root') as Element
          )
          : null
      }
    </>
  );
}
export default Modal;