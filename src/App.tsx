/* eslint-disable @typescript-eslint/no-explicit-any */
import { FormikHelpers } from 'formik'
import './App.css'
import Modal from './components/Modal'
import TablaTrabajador from './components/TablaTrabajador'
import TrabajadorForm from './components/TrabajadorForm'
import useModal from './hooks/useModal'
import { ITrabajador, ITrabajadorById, ITrabajadorForm } from './interfaces/trabajador.interface'
import { trabajadorSchemeValidatior } from './validations/trabajador.validation'
import { useEffect, useState } from 'react'
import trabajadorService from './services/trabajador.service'
import Swal from 'sweetalert2'

function App() {
  const [trabajadores, setTrabajadores] = useState<ITrabajador[] | null>(null)

  const { isOpen: isOperRegisterModal, openModal: openRegisterModal, closeModal: closeRegisterModal } = useModal()
  const { isOpen: isOperUpdateModal, openModal: openUpdateModal, closeModal: closeUpdateModal } = useModal()

  const createInitialValues: ITrabajadorForm = {
    tipoDocumento: '',
    numeroDocumento: '',
    nombres: '',
    sexo: '',
    idDepartamento: 0,
    idProvincia: 0,
    idDistrito: 0
  }

  const handleCreateSubmit = async (values: ITrabajadorForm, helpers: FormikHelpers<ITrabajadorForm>) => {
    try {
      const trabajador = await trabajadorService.addTrabajador(values)
      const newList = trabajadores ? [...trabajadores, trabajador] : [trabajador]
      setTrabajadores(newList)
      closeRegisterModal()
      Swal.fire({
        icon: 'success',
        text: 'Registro exitoso'
      })
    } catch (error) {
      Swal.fire({
        icon: 'error',
        text: 'Error al registrar'
      })
    } finally {
      helpers.setSubmitting(false)
    }
  }

  //Update trabajador
  const [selectedTrabajador, setSelectedTrabajador] = useState<ITrabajadorById>()

  const handleUpdate = async (id: number) => {
    const trabajador = await trabajadorService.getTrabajadorById(id.toString())
    setSelectedTrabajador(trabajador)
    openUpdateModal()
  }

  const updateInitialValues: ITrabajadorForm = {
    tipoDocumento: selectedTrabajador?.tipoDocumento ?? '',
    numeroDocumento: selectedTrabajador?.numeroDocumento ?? '',
    nombres: selectedTrabajador?.nombres ?? '',
    sexo: selectedTrabajador?.sexo ?? '',
    idDepartamento: selectedTrabajador?.idDepartamento ?? '',
    idProvincia: selectedTrabajador?.idProvincia ?? '',
    idDistrito: selectedTrabajador?.idDistrito ?? ''
  }

  const handleUpdateSubmit = async (values: ITrabajadorForm, helpers: FormikHelpers<ITrabajadorForm>) => {
    try {
      const trabajador = await trabajadorService.updateTrabajador(values, selectedTrabajador?.id.toString() ?? '')
      const newList = trabajadores && trabajadores.map(t => t.id == selectedTrabajador?.id ? trabajador : t)
      setTrabajadores(newList || trabajadores)
      closeUpdateModal()
      Swal.fire({
        icon: 'success',
        text: 'Actualización exitosa'
      })
    } catch (error) {
      Swal.fire({
        icon: 'error',
        text: 'Error al actualizar'
      })
    } finally {
      helpers.setSubmitting(false)
    }
  }

  //Delete trabajador
  const handleDeleteTrabajador = async (id: number) => {
    try {
      Swal.fire({
        title: '¿Estas seguro de eliminar al trabajador?',
        text: "Esta acción no es reversible!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, elimínalo'
      }).then(async (result) => {
        if (result.isConfirmed) {
          const trabajadorEliminado = await trabajadorService.deletTrabajadorById(id.toString())
          const newList = trabajadores?.filter(t => t.id !== trabajadorEliminado.id)
          setTrabajadores(newList || trabajadores)
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
      })
    } catch (error) {
      Swal.fire({
        icon: 'error',
        text: 'Error al eliminar'
      })
    }
  }

  //Filtrar por sexo
  const filtrarPorSexo = async (e: any) => {
    const sexo = e.target.value
    if (sexo === ""){
      const trabajadores = await trabajadorService.getTrabajadores();
      setTrabajadores(trabajadores)
    }else {
      const trabajadores = await trabajadorService.getTrabajadorBySexo(sexo);
      setTrabajadores(trabajadores)
    }
  }

  useEffect(() => {
    trabajadorService.getTrabajadores().then(t => setTrabajadores(t))
  }, [setTrabajadores])

  return (
    <>
      <header className='d-flex justify-content-center bg-dark'>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark container">
          <div className="container-fluid">
            <a className="navbar-brand fs-4" href="/">Prueba Trabajadores</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link active" href="/">Home</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <main className='container my-4'>
        <h1 className='display-3'>Gestión de trabajadores</h1>
        <div className='my-4'>
          <div className='d-flex mb-4 gap-4 mt-4'>
            <button className='btn btn-primary' onClick={openRegisterModal}>Nuevo registro</button>
            <select name="sexo" className='form-select w-auto' onChange={(e) => filtrarPorSexo(e)}>
              <option value="">Todos</option>
              <option value="M">Masculino</option>
              <option value="F">Femenino</option>
            </select>
          </div>
          <TablaTrabajador openModal={handleUpdate} trabajadores={trabajadores} handleDelete={handleDeleteTrabajador} />
          {/* Modal de Registro */}
          <Modal isOpen={isOperRegisterModal}>
            <h3 className='text-center fw-normal fs-2 mb-4'>Registrar Trabajador</h3>
            <TrabajadorForm handleSubmit={handleCreateSubmit} initialValues={createInitialValues} validationSchema={trabajadorSchemeValidatior} handleClose={closeRegisterModal} />
          </Modal>

          {/*Medal de Actualización*/}
          <Modal isOpen={isOperUpdateModal}>
            <h3 className='text-center fw-normal fs-2 mb-4'>Actualizar trabajador</h3>
            <TrabajadorForm handleSubmit={handleUpdateSubmit} initialValues={updateInitialValues} validationSchema={trabajadorSchemeValidatior} handleClose={closeUpdateModal} />
          </Modal>
        </div>
      </main>
      <footer className='container-fluid bg-dark d-flex justify-content-center  align-items-center p-2'>
        <p className='text-white m-0 fs-5 fw-light'>Gestión de trabajadores</p>
      </footer>
    </>
  )
}

export default App
