export interface IDepartamento {
  id: number
  nombreDepartamento: string
}

export interface IProvinica {
  id: number
  nombreProvincia: string
}

export interface IDistrito {
  id: number
  nombreDistrito: string
}