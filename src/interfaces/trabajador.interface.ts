
export interface ITrabajador {
  id: number
  tipoDocumento: string
  numeroDocumento: string
  nombres: string
  sexo: string
  nombreDepartamento: string
  nombreProvincia: string
  nombreDistrito: string
}

export interface ITrabajadorForm {
  tipoDocumento: string
  numeroDocumento: string
  nombres: string
  sexo: string
  idDepartamento: number | string
  idProvincia: number | string
  idDistrito: number | string
}

export interface ITrabajadorById extends ITrabajadorForm {
  id: number
}